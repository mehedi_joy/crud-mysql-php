<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <title>Update</title>
</head>
<body>
    <?php
        require_once("vendor/autoload.php");
        $user_obj = new Joy\User();
        $getID = $_GET["id"];
        if($_SERVER["REQUEST_METHOD"] == "POST") {
            $user_obj->update_user($_POST, $getID);
            header("Location:index.php");
        } else {
            $result = $user_obj->get_one($getID);
        }
        $conn = null;
    ?>
    <div class="container">
        <div class="cnt" style="width: 30vw;margin: auto;">
        <form action="./update.php?id=<?= $result['id']?>" method="POST">
            <input type="text" name="id" id="id" hidden value="<?= $result['id']?>">
            <label class="form-label" for="name">Name:</label>
            <input class="form-control" type="text" name="name" id="name" value="<?= $result['name']?>"><br>
            <label class="form-label" for="email">Email:</label>
            <input class="form-control" type="text" name="email" id="email" value="<?= $result['email']?>"><br>
            <label class="form-label" for="phone">Phone:</label>
            <input class="form-control" type="number" name="phone" id="phone" value="<?= $result['phone']?>"><br>
            <label class="form-label" for="address">Address:</label>
            <input class="form-control"type="text" name="address" id="address" value="<?= $result['address']?>"><br>
            <input class="btn btn-primary" type="submit" value="Update">
            <a href="index.php" class="btn btn-outline-secondary">Go Back</a>
        </form>
        </div>
    </div>

</body>
</html>