<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <title>Users Info</title>
</head>

<body>
    <?php
        require_once("vendor/autoload.php");
        $user_obj = new Joy\User();
        $result = $user_obj->get_all();
    ?>
    <div class="container mt-3">
        <table class="table" border="1">
            <tbody>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Action</th>
                </tr>
                <?php
                    foreach ($result as $key => $row) {
                        echo<<<PDO
                        <tr>
                        <td>{$row['id']}</td>
                        <td>{$row['name']}</td>
                        <td>{$row['email']}</td>
                        <td>{$row['phone']}</td>
                        <td>{$row['address']}</td>
                        <td>
                            <a class="btn btn-outline-primary" href="show.php?id={$row['id']}">Show</a>
                            <a class="btn btn-outline-primary" href="update.php?id={$row['id']}">Edit</a>
                            <a class="btn btn-outline-primary" href="delete.php?id={$row['id']}">Delete</a>
                        </td>
                        <tr>
                        PDO;
                    }
                ?>
            </tbody>
        </table>
        <a class="btn btn-primary" href="./create.php">Add User</a>
    </div>
    
</body>

</html>