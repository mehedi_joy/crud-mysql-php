<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <title>Update</title>
</head>
<body>
    <?php
        require_once("vendor/autoload.php");
        if($_SERVER["REQUEST_METHOD"] == "POST") {
            $user_obj = new Joy\User();
            $user_obj->store_user($_POST);
            header("Location:index.php");
        }
        

    ?>
    <form style="margin: auto;width: 30vw;" action="create.php" method="POST">
        <label class="form-label" for="name">Name:</label>
        <input class="form-control" type="text" name="name" id="name"><br>
        <label class="form-label" for="email">Email:</label>
        <input class="form-control" type="text" name="email" id="email"><br>
        <label class="form-label" for="phone">Phone:</label>
        <input class="form-control" type="number" name="phone" id="phone"><br>
        <label class="form-label" for="address">Address:</label>
        <input class="form-control" type="text" name="address" id="address"><br>
        <input class="btn btn-outline-primary" type="submit" value="Save">
    </form>
</body>
</html>