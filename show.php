<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <title>User</title>
</head>
<body>
    <?php
        require_once("vendor/autoload.php");
        $user_obj = new Joy\User();
        $getID = $_GET['id'];
        $result = $user_obj->get_one($getID);
    ?>
    <div class="container">
        <div class="cnt" style="width: 30vw;margin: auto;">
            <p><b>Name:</b> <?=$result['name']?></p>
            <p><b>Email:</b> <?=$result['email']?></p>
            <p><b>Phone:</b> <?=$result['phone']?></p>
            <p><b>Address:</b> <?=$result['address']?></p>
            <a href="index.php" class="btn btn-outline-secondary">Go Back</a>  
        </div>
    </div>
</body>
</html>