<?php
namespace Joy;

class User{ 
    public $servername = "localhost";
    public $username = "root";
    public $db_password = "";
    public $dbname = "joy";
    public $conn;

    function __construct() {
        try {
            $this->conn = new \PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->db_password);
            $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch(\PDOException $e) {
            echo $e->getMessage();
        }    
    }

    function get_all() {
        $stmt = $this->conn->prepare("SELECT * FROM users");
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $result;
    }

    function get_one($getID) {
        $id = $getID;
        $stmt = $this->conn->prepare("SELECT * FROM users WHERE id='$id'");
        $stmt->execute();
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }

    function store_user($data) {
        $name = $data["name"];
        $email = $data["email"];
        $phone = $data["phone"];
        $address = $data["address"];
        
        $stmt = $this->conn->prepare("INSERT INTO users (name, email, phone, address) VALUES ('$name','$email','$phone','$address')");
        $stmt->execute();
    }

    function update_user($data, $getID) {
        $id = $getID;
            
        $name = $data["name"];
        $email = $data["email"];
        $phone = $data["phone"];
        $address = $data["address"];
        
        $stmt = $this->conn->prepare("UPDATE users SET name='$name', email='$email', phone='$phone', address='$address' WHERE id=$id");
        $stmt->execute();
    }

    function delete_user($getID) {
        $id = $getID;
        $stmt = $this->conn->prepare("DELETE FROM users WHERE id='$id'");
        $stmt->execute();
    }

}